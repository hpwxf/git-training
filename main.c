#include <stdio.h>
#include <stdlib.h>

int isLucky() {
  // Get a double precision random number
  return (drand48() <= 0.5);
}


int main() {
  // Sample code
  printf("Hello git\n"); 
  printf("\tgit is funny!\n");
  printf("Are you lucky ?\n");
  if (isLucky()) {
    printf("\tYES you are !!!!!\n");
  } else {
    printf("\tmaybe next time; try again.\n");
  }
}
